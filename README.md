**Atividade - Disciplina Modelagem e Desenvolvimento Orientado a Objetos**

### Grupo
* André 
* Fernando Souza *[Twitter](https://www.twitter.com/morphors)*
* Kaique Souza
* João Pedro
* Ygor Fabricio

---

## Atividade 1

Criar diagrama de atividades com base no diagrama de Caso de Uso dado.

1. Validar Login
2. Cadastrar funcionários
3. Atualizar dados do funcionário
4. Remover funcionário
5. Pesquisar funcionário
6. Criar diagrama da Classe funcionário (**atributos** e **métodos**)

---

## Atividade 2

Desenvolva um sistema em **JAVA** observando os diagramas realizados.

1. Criar classe funcionário
2. Função login
3. Menu para funções
4. Cadastrar funcionário
5. Pesquisar funcionário
6. Editar funcionário
7. Remover funcionário

---

