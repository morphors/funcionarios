package prova;


import java.util.Scanner;
import prova.Funcionario;

public class Principal {

	public static void main(String[] args) {

		Scanner leitor = new Scanner(System.in);
		boolean loginAtivo = true;
		String usuario = "admin";
		String senha = "admin";
		
		Funcionario cadastro = new Funcionario();
		cadastro.cadastrarexemplos();

// Inicio Login no sistema
		System.out.println("\t\t Acessar Sistema de Controle de Funcionarios \n");
		System.out.print("Nome: ");
		String lerNome = leitor.next();

		System.out.print("Senha: ");
		String lerSenha = leitor.next();

		if(lerNome.equals(usuario) && lerSenha.equals(senha)) {
			loginAtivo = true;
		} else {
			System.out.println("\t Usuario ou senha invalido... Encerrando ");
			loginAtivo = false;
		} // Final Login no Sistema

		while(loginAtivo){
			System.out.println("");
			System.out.println(
					"+---------- SISTEMA DE FUNCIONARIOS ----------+\n" +
					"|Digite:                                      |\n" +
					"|                                             |\n" +
					"| 1 Pesquisar Funcionario                     |\n" +
					"| 2 Cadastrar                                 |\n" +
					"| 3 Editar                                    |\n" +
					"| 4 Remover                                   |\n" +
					"| 5 Mostrar Todos                             |\n" +
					"|                                             |\n" +
					"| 99 Sair                                     |\n" +
					"+---------------------------------------------+");

			int opcao = leitor.nextInt();

			switch (opcao) {
			case 1:
				System.out.println("Pesquisa de Funcionario!!!\n");
				cadastro.buscaFuncionario();
				break;

			case 2:
				System.out.println("Cadastrar Funcionario!!!\n");
				cadastro.cadastrarFuncionarios();
				System.out.println("\nFuncionário Cadastrado!!!\n");
				break;

			case 3:
				System.out.println("Editar Funcionario!!!\n");
				cadastro.mostrarFuncionarios();
				cadastro.editarfuncionario();
				
				break;

			case 4:
				System.out.println("Remover Funcionario!!!\n");
				cadastro.mostrarFuncionarios();
				cadastro.removerFuncionario();
				break;

			case 5: 
				System.out.println("Todos os Funcionarios Cadastrados\n");
				cadastro.mostrarFuncionarios();
				break;                        

			case 99:
				System.out.println("Saindo...\n");
				loginAtivo = false;
				System.out.println("\nSistema Encerrado.\n");
				break;

			default:
				System.out.println("Opcao Invalida\n");
				break;
			}

		}
		leitor.close();

	}

}

