package prova;

import java.util.Scanner;
import java.util.ArrayList;

public class Funcionario {
	private String nome;
	private String cpf;
	private String funcao;
	private double salario;

	Scanner leitor = new Scanner(System.in).useDelimiter("\n");
	ArrayList<Funcionario> listaFuncionarios = new ArrayList<Funcionario>();

	public Funcionario(String nome, String cpf, String funcao, double salario ) {
		this.nome = nome;
		this.cpf = cpf;
		this.funcao = funcao;
		this.salario = salario;
	}
	public Funcionario() {

	}

	// Getters and Setters
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;	
	}
	public String getFuncao() {
		return funcao;
	}
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}

	// Metodos funcionarios
	public void mostrarFuncionarios() {
		System.out.println("============= LISTA DE FUNCIONARIOS ======================");
		for(Funcionario lista:listaFuncionarios) {
			System.out.println("COD: " +listaFuncionarios.indexOf(lista) + "\t|" 
					+ "" + lista.getNome()
					+ "\t |"+ lista.getFuncao()
					+ "\t |"+ lista.getCpf()
					+ " | R$: "+ lista.getSalario() + "|");
		}
	}
	public void editarfuncionario(){
		System.out.println("Qual codigo do usuario que deseja editar:");
		int editar = leitor.nextInt();
		if(editar < listaFuncionarios.size()){
			System.out.println("- Digite os novo valores ");
			System.out.println("Qual o novo nome para o funcionario: ");
			String enome = leitor.next();
			System.out.println("Qual o novo CPF: ");
			String ecpf = leitor.next();
			System.out.println("Qual a nova funcao: ");
			String ecargo = leitor.next();
			System.out.println("Qual o novo salario: ");		
			Double esalario = leitor.nextDouble();
			Funcionario efuncionario = new Funcionario(enome,ecpf,ecargo,esalario);
	
			listaFuncionarios.set(editar, efuncionario);
			System.out.println("Dados do Funcionario atualizados com sucesso");
		
		}else{
			System.out.println("Funcionario nao localizado.");
		}
	}
	
	public void cadastrarFuncionarios() {
		Funcionario novo = new Funcionario();
		System.out.println("Digite o nome do funcionario: ");
		novo.setNome(leitor.next());
		System.out.println("Digite o CPF do funcionario: ");
		novo.setCpf(leitor.next());
		System.out.println("Digite a cargo do funcionario: ");
		novo.setFuncao(leitor.next());
		System.out.println("Digite o salario do funcionario: ");
		novo.setSalario(leitor.nextDouble());
		listaFuncionarios.add(novo);
	}
	public void removerFuncionario() {
		System.out.println("Digite o cod do funcionario");
		int apagar = leitor.nextInt();
		if(apagar < listaFuncionarios.size()){
			System.out.println("Deseja realmente apagar o cadastro abaixo");
			System.out.println("Nome: " +listaFuncionarios.get(apagar).nome);
			System.out.println("CPF: " +listaFuncionarios.get(apagar).cpf);
			System.out.println("Responda com \"s\" para confirmar.");
			if(leitor.next().equals("s")) {
				listaFuncionarios.remove(apagar);
				System.out.println("Cadastro Apagado");
			}else {
				System.out.println("Cancelado remocao.");
			}
		}else{
			System.out.println("Funcionario nao localizado.");
		}
	}
	// EXEMPLOS
	public void cadastrarexemplos() {
		listaFuncionarios.add(new Funcionario("Kevin Mitnick", "333.333.333-30", "Analista", 8000));
		listaFuncionarios.add(new Funcionario("Steve Wozniak", "222.222.222-22", "Engenheiro", 10000));
		listaFuncionarios.add(new Funcionario("Bill H. Gates", "444.444.444-44", "Gerente OP", 7200));
	}
	
	public void buscaFuncionario() {
		System.out.println("Qual o nome do funcionário que deseja buscar?");
		String nomeBuscar = leitor.next();
		Funcionario pessoaPeloNome = buscaPeloNome(listaFuncionarios, nomeBuscar);

		if(pessoaPeloNome != null) {
			System.out.println("Funcionário encontrado "
					+ "\n|COD: " +listaFuncionarios.indexOf(pessoaPeloNome)
					+ "\t |" + pessoaPeloNome.getNome()
					+ "\t |" + pessoaPeloNome.getCpf()
					+ "\t |" + pessoaPeloNome.getFuncao()+ "|");
		}else {
			System.out.println("O nome" +nomeBuscar + " não foi localizado.");
		}
	}
	public static Funcionario buscaPeloNome(ArrayList<Funcionario> funcionarios, String nome) {

        for(Funcionario funcionario : funcionarios) {
            if(funcionario.getNome().contains(nome)) {
                return funcionario;
            }
        }
        return null;
    }

}
